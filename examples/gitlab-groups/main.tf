variable "gitlab_token" {}
variable "gitlab_groups" {}

provider "gitlab" {
  token = var.gitlab_token
}

terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.4.0"
    }
  }
  experiments = [module_variable_optional_attrs]
}


module "gitlab_group" {
  count = length(var.gitlab_groups)
  source           = "../../modules/group"
  name             = lookup(var.gitlab_groups[count.index], "name")
  path             = lookup(var.gitlab_groups[count.index], "path")
  description      = lookup(var.gitlab_groups[count.index], "description")
  visibility_level = lookup(var.gitlab_groups[count.index], "visibility_level")
  users = lookup(var.gitlab_groups[count.index], "users")
  variables = lookup(var.gitlab_groups[count.index], "variables", [])
}
