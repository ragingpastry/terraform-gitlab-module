module gitlab.com/ragingpastry/terraform-gitlab-module/modules/group

go 1.13

require (
	github.com/gruntwork-io/terratest v0.30.27
	github.com/magefile/mage v1.11.0 // indirect
	github.com/stretchr/testify v1.4.0
)
