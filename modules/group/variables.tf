variable "name" {
    type = string
}

variable "path" {
    type = string
}

variable "description" {
    type = string
}

variable "visibility_level" {
    type = string
    description = "Visibility level of the group. Must be either private, internal, or public"
    default = "private"

    validation {
        condition = var.visibility_level == "public" || var.visibility_level == "internal" || var.visibility_level == "public"
        error_message = "Visibility_level must be either public, private, or internal."
    }
}

variable "project_creation_level" {
    type = string
    description = "Determines if developers can create projects in the group"
    default = "maintainer"

    validation {
        condition = var.project_creation_level == "noone" || var.project_creation_level == "maintainer" || var.project_creation_level == "developer"
        error_message = "Project_creation_level must be 'noone or 'maintainer'."
    }
}

variable "auto_devops_enabled" {
    type = bool
    default = false
}

variable "subgroup_creation_level" {
    type = string
    description = "Determines who is allowed to create subgroups. Must be one of 'owner' or 'maintainer'"
    default = "owner"

    validation {
        condition = var.subgroup_creation_level == "maintainer" || var.subgroup_creation_level == "owner"
        error_message = "Subgroup_creation_level must be 'owner' or 'maintainer'."
    }
}

variable "require_two_factor_authentication" {
    type = bool
    description = "Required all uses in the group to configure two-factor authentication"
    default = false
}

variable "parent_group_id" {
    type = number
    default = null
}

variable "users" {
    type = list(object({
        username = string,
        access_level = string
    }))

    description = "Users to add to the group"

    default = []
}

variable "variables" {
    type = list(object({
        key = string,
        value = string,
        protected = optional(bool),
        masked = optional(bool)
    }))
}
