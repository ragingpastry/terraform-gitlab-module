output "project_id" {
  value = gitlab_project.project.id
  depends_on = [gitlab_project.project]
}

output "ssh_url_to_repo" {
  value = gitlab_project.project.ssh_url_to_repo
  depends_on = [gitlab_project.project]
}

output "http_url_to_repo" {
  value = gitlab_project.project.http_url_to_repo
  depends_on = [gitlab_project.project]
}

output "web_url" {
  value = gitlab_project.project.web_url
  depends_on = [gitlab_project.project]
}

output "runners_token" {
  value = gitlab_project.project.runners_token
}
