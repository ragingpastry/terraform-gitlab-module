package test

import (
	"testing"

	"github.com/gruntwork-io/terratest/modules/terraform"
	"github.com/stretchr/testify/assert"
)

func TestTerraformBasicExample(t *testing.T) {
	t.Parallel()
	expectedPath := "ragingpastry-test-group-1234"
	expectedWebUrl := "https://gitlab.com/groups/ragingpastry-test-group-1234"

	terraformOptions := terraform.WithDefaultRetryableErrors(t, &terraform.Options{
		TerraformDir: "../examples/gitlab-groups",

		VarFiles: []string{"gitlab-groups.tfvars"},

		NoColor: true,
	})

	defer terraform.Destroy(t, terraformOptions)

	terraform.InitAndApply(t, terraformOptions)

	actualPath := terraform.Output(t, terraformOptions, "full_path")
	actualWebUrl := terraform.Output(t, terraformOptions, "web_url")

	assert.Contains(t, actualPath, expectedPath)
	assert.Contains(t, actualWebUrl, expectedWebUrl)

}
