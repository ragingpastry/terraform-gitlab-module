gitlab_groups = [
    {
        name = "ragingpastry-test-group-1234",
        path = "ragingpastry-test-group-1234",
        description = "This is my group",
        visibility_level = "public",
        users = [
            {
                username = "mbank59",
                access_level = "developer"
            }
        ],
        variables = [
            {
                key = "test",
                value = "test",
                protected = false,
                masked = false
            }
        ]
    }
]
