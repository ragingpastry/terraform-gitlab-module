terraform {
    required_providers {
        gitlab = {
            source = "gitlabhq/gitlab"
            version = "3.4.0"
        }
    }
    experiments = [module_variable_optional_attrs]
}

resource "gitlab_group" "group" {
    name = var.name
    path = var.path
    description = var.description
    visibility_level = var.visibility_level
    project_creation_level = var.project_creation_level
    auto_devops_enabled = var.auto_devops_enabled
    subgroup_creation_level = var.subgroup_creation_level
    require_two_factor_authentication = var.require_two_factor_authentication
    parent_id = var.parent_group_id
}

data "gitlab_user" "user" {
    count = length(var.users)
    username = lookup(var.users[count.index], "username")
}

resource "gitlab_group_membership" "group_membership" {
    count = length(var.users)
    group_id = gitlab_group.group.id
    user_id = data.gitlab_user.user[count.index].id
    access_level = lookup(var.users[count.index], "access_level", "developer")
}

resource "gitlab_group_variable" "group_variable" {
    count = length(var.variables)
    group = gitlab_group.group.id
    key = lookup(var.variables[count.index], "key")
    value = lookup(var.variables[count.index], "value")
    protected = lookup(var.variables[count.index], "protected", false)
    masked = lookup(var.variables[count.index], "masked", false)
}
