variable "name" {}

variable "group_id" {
    default = ""
}

variable "description" {
    default = ""
}

variable "visibility" {
    default = "public"
}

variable "enable_branch_protection" {
    default = 1
}

variable "default_branch" {
    default = "master"
}


variable "enable_merge_request_approvals" {
    default = 0
}

variable "approvals_before_merge" {
    default = 1
}

variable "only_allow_merge_if_pipeline_succeeds" {
    default = "true"
}

variable "merge_request_approval_rules" {
    type = list(object({
        name = string,
        num_approvals = number,
        user_ids = list(number),
        group_ids = optional(list(number))
    }))
    default = []
}
