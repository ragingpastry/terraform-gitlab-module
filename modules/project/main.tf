terraform {
  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "3.4.0"
    }
  }
  experiments = [module_variable_optional_attrs]
}

resource "gitlab_project" "project" {
    name = var.name
    namespace_id = var.group_id
    description = var.description
    visibility_level = var.visibility
    default_branch = var.default_branch
    approvals_before_merge = var.approvals_before_merge
    only_allow_merge_if_pipeline_succeeds = var.only_allow_merge_if_pipeline_succeeds
}
resource "gitlab_branch_protection" "branch_protection" {
    count = var.enable_branch_protection
    project = gitlab_project.project.id
    branch = "master"
    merge_access_level = "developer"
    push_access_level = "master"

    depends_on = [gitlab_project.project]
}

resource "gitlab_project_approval_rule" "approval_rule" {
    count = length(var.merge_request_approval_rules)
    project = gitlab_project.project.id
    name = lookup(var.merge_request_approval_rules[count.index], "name", "")
    approvals_required = lookup(var.merge_request_approval_rules[count.index], "num_approvals", "")
    user_ids = lookup(var.merge_request_approval_rules[count.index], "user_ids", [])
    group_ids = lookup(var.merge_request_approval_rules[count.index], "group_ids", [])

    depends_on = [gitlab_project.project]
}
