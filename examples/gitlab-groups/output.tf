output "full_path" {
    value = module.gitlab_group.*.full_path
}

output "web_url" {
    value = module.gitlab_group.*.web_url
}
