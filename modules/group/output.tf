output "group_id" {
  value = gitlab_group.group.id
  depends_on = [gitlab_group.group]
}

output "full_path" {
    value = gitlab_group.group.full_path
    depends_on = [gitlab_group.group]
}

output "full_name" {
    value = gitlab_group.group.full_path
    depends_on = [gitlab_group.group]
}

output "web_url" {
    value = gitlab_group.group.web_url
    depends_on = [gitlab_group.group]
}

output "runners_token" {
    value = gitlab_group.group.runners_token
    depends_on = [gitlab_group.group]
}
